FROM golang:1.12

WORKDIR /go/src/gitlab.com/tokend/createAccount

COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -o /usr/local/bin/createAccount gitlab.com/tokend/createAccount


###

FROM alpine:3.9

COPY --from=0 /usr/local/bin/createAccount /usr/local/bin/createAccount
RUN apk add --no-cache ca-certificates

ENTRYPOINT ["createAccount"]
