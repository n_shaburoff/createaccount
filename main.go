package main

import (
	"os"

  "gitlab.com/tokend/createAccount/internal/cli"

)

func main() {
	if !cli.Run(os.Args) {
		os.Exit(1)
	}
}
