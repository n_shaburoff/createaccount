/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type ResourceType string

// List of ResourceType
const (
	IDENTITY_ACCOUNT ResourceType = "identity-account"
	SIGNER           ResourceType = "signer"
)
