package postgres

import (
	sq "github.com/Masterminds/squirrel"
	"github.com/fatih/structs"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/tokend/createAccount/internal/data"
)

type AccountSigners struct {
	db  *pgdb.DB
	sql sq.SelectBuilder
}

func (q *AccountSigners) New() data.AccountSigners {
	return NewAccountSigners(q.db.Clone())
}

func (q *AccountSigners) Create(signer data.AccountSigner) (data.AccountSigner, error) {
	entity := structs.Map(signer)
	var result data.AccountSigner
	stmt := sq.Insert("account_signers").SetMap(entity).Suffix("returning *")
	err := q.db.Get(&result, stmt)
	return result, err
}

func (q *AccountSigners) Select() ([]data.AccountSigner, error) {
	var result []data.AccountSigner
	err := q.db.Select(&result, q.sql)
	return result, err
}

var accountSignersSelect = sq.
	Select("a.*").
	From("account_signers a")

func NewAccountSigners(db *pgdb.DB) data.AccountSigners {
	return &AccountSigners{
		db: db.Clone(),
		sql:  accountSignersSelect,
	}
}




