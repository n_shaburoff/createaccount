package data

type AccountSigners interface {
	New() AccountSigners

	Create(signer AccountSigner) (AccountSigner, error)
	Select() ([]AccountSigner, error)
	
}


type AccountSigner struct {
	WalletID string `db:"wallet_id" structs:"-"`
	SignerID string	`db:"signer_id" structs:"signer_id"`
	RoleID   uint64 `db:"role_id" 	structs:"role_id"`
	Weight   uint32	`db:"weight" 	structs:"weight"`
	Identity uint32 `db:"identity"	structs:"identity"`
}

