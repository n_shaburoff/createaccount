package accountcreator

import (
	"context"
	"encoding/json"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/createAccount/internal/data"
	"gitlab.com/tokend/createAccount/internal/helpers"
	"gitlab.com/tokend/createAccount/internal/utils"
	"gitlab.com/tokend/go/xdrbuild"
)

type AccountCreator struct {
	Tx *xdrbuild.Transaction
}

type AccountDetails struct {
	AccountID string
	Signers   []data.AccountSigner
}

func New(tx *xdrbuild.Transaction) AccountCreator {
	return AccountCreator{
		Tx: tx,
	}
}

func (с AccountCreator) CreateAccount(ctx context.Context, accountID string, signers []data.AccountSigner) error {
	tx := с.Tx
	tx, _, err := с.account(tx, accountID, signers)
	if err != nil {
		return errors.Wrap(err, "failed to craft account operation")
	}
	_, err = tx.Marshal()
	if err != nil {
		return errors.Wrap(err, "failed to build tx envelope")
	}

	return nil
}

func (c AccountCreator) account(tx *xdrbuild.Transaction, accountID string, accountSigners []data.AccountSigner) (*xdrbuild.Transaction, uint64, error) {
	accountRole := utils.Uint64()

	tx = tx.Op(&xdrbuild.CreateAccount{
		Destination: accountID,
		RoleID: accountRole,
		Signers: c.getSigners(accountSigners),
	})

	return tx, accountRole, nil
}

func (c AccountCreator) getSigners(accountSigners []data.AccountSigner) []xdrbuild.SignerData {
	var signers []xdrbuild.SignerData
	for _, accountSigner := range accountSigners {
		signers = append(signers, xdrbuild.SignerData{
			PublicKey: accountSigner.SignerID,
			RoleID:    accountSigner.RoleID,
			Weight:    accountSigner.Weight,
			Identity:  accountSigner.Identity,
			Details:   Details{},
		})
	}
	return signers
}
type Details map[string]interface{}

func (d Details) MarshalJSON() ([]byte, error) {
	return json.Marshal(map[string]interface{}(d))
}
