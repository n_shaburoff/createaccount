package handlers

import (
    "context"
    "gitlab.com/distributed_lab/logan/v3"
    "gitlab.com/tokend/createAccount/internal/accountcreator"
    "gitlab.com/tokend/createAccount/internal/data"
    "gitlab.com/tokend/go/xdrbuild"
    "gitlab.com/tokend/keypair"
    "gitlab.com/tokend/regources"
    "net/http"
)

type ctxKey int

const (
    logCtxKey ctxKey = iota
    txBuilderCtxKey
    coreInfoCtxKey
    AccountQCtxKey
)

func AccountCreator(r *http.Request) accountcreator.AccountCreator {
    txbuilder := r.Context().Value(txBuilderCtxKey).(Infobuilder)
    info := r.Context().Value(coreInfoCtxKey).(Info)
    master := keypair.MustParseAddress(CoreInfo(r).MasterAccountID)
    tx := txbuilder(info, master)

    return accountcreator.New(
        tx,
    )
}


func CtxLog(entry *logan.Entry) func(context.Context) context.Context {
    return func(ctx context.Context) context.Context {
        return context.WithValue(ctx, logCtxKey, entry)
    }
}

func Log(r *http.Request) *logan.Entry {
    return r.Context().Value(logCtxKey).(*logan.Entry)
}

func CtxCoreInfo(s Info) func(context.Context) context.Context {
    return func(ctx context.Context) context.Context {
        return context.WithValue(ctx, coreInfoCtxKey, s)
    }
}

func CoreInfo(r *http.Request) *regources.Info {
    info, err := r.Context().Value(coreInfoCtxKey).(Info).Info()
    if err != nil {
        //TODO handle error
        panic(err)
    }
    return info
}



type Infobuilder func(info Info, source keypair.Address) *xdrbuild.Transaction

type Info interface {
    Info() (*regources.Info, error)
}

func CtxAccountQ(q data.AccountSigners) func(context.Context) context.Context {
    return func(ctx context.Context) context.Context {
        return context.WithValue(ctx, AccountQCtxKey, q)
    }
}

func AccountQ(r *http.Request) data.AccountSigners {
    return r.Context().Value(AccountQCtxKey).(data.AccountSigners).New()
}



