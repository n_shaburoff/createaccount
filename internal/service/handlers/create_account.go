package handlers

import (
	validation "github.com/go-ozzo/ozzo-validation"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/createAccount/internal/data"
	"gitlab.com/tokend/createAccount/internal/service/requests"
	"gitlab.com/tokend/createAccount/resources"
	"net/http"
)

func CreatAccount(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewCreateAccountRequest(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	signers, err := getSigners(request)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	err = AccountCreator(r).CreateAccount(r.Context(), request.Data.ID, signers)
	if err != nil {
		Log(r).WithError(err).Error("failed to create account")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	w.WriteHeader(http.StatusCreated)

}

func getSigners(request resources.CreateAccountResponse) ([]data.AccountSigner, error) {
	var signers []data.AccountSigner
	for _, signerKey := range request.Data.Relationships.Signers.Data {
		signer := request.Included.MustSigner(signerKey)
		if signer == nil {
			return nil, validation.Errors{"/included": errors.New("missed signer include")}
		}
		signers = append(signers, data.AccountSigner{
			SignerID: signerKey.ID,
			RoleID:   uint64(signer.Attributes.RoleId),
			Weight:   uint32(signer.Attributes.Weight),
			Identity: uint32(signer.Attributes.Identity),
		})
	}
	return signers, nil
}
